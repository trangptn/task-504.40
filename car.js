class Car{
    __make;
    __model;

    constructor(paramMake,paramModel)
    {
        this.__make=paramMake;
        this.__model=paramModel;
    }

    drive()
    {
        return this.__make+" - "+this.__model;
    }

}
export {Car}