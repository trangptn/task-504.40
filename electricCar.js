import { Car } from "./car.js";
class ElectricCar extends Car{
    __batteryLevel;

    constructor(paramBattery)
    {
        super(paramBattery);
        this.__batteryLevel=paramBattery;
    }

    charge()
    {
        return this.__batteryLevel;
    }
}
export {ElectricCar}