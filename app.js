import { Car } from "./car.js";
import { ElectricCar } from "./electricCar.js";
import { PetrolCar } from "./petrolCar.js";

var car1=new Car("oto","new");
console.log(car1.drive());

var electriccar1=new ElectricCar("abcd");
console.log(electriccar1.charge());

var petrolCar1= new PetrolCar("r92");
console.log(petrolCar1.fillUp());

console.log(car1 instanceof Car);
console.log(car1 instanceof ElectricCar);
console.log(car1 instanceof PetrolCar);

console.log(electriccar1 instanceof Car);
console.log(electriccar1 instanceof ElectricCar);
console.log(electriccar1 instanceof PetrolCar);

console.log(petrolCar1 instanceof Car);
console.log(petrolCar1 instanceof ElectricCar);
console.log(petrolCar1 instanceof PetrolCar);