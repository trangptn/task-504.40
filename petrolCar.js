import { Car } from "./car.js";

class PetrolCar extends Car{
    __fuelLevel;

    constructor(paramFuel)
    {
        super(paramFuel);
        this.__fuelLevel=paramFuel;
    }

    fillUp()
    {
        return this.__fuelLevel;
    }
}
export {PetrolCar}